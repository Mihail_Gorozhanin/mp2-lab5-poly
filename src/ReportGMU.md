# Методы программирования **2**
***
#Лабораторная работа №**5**: Полиномы
***

##Введение:
***
Лабораторная работа направлена на практическое освоение структуры данных **Списки**.

С этой целью в лабораторной работе изучаются различные варианты структуры хранения очереди и разрабатываются методы и программы решения задач с использованием списков. В качестве области приложений выбрана тема выполнения математическая модель – *алгебра полиномов*. 

Под полиномом от одной переменной понимается выражение вида:
![](http://s019.radikal.ru/i616/1705/23/7f29bf380f6e.png)
или в более общем виде
![](http://s015.radikal.ru/i333/1705/c8/65223fbe7ebd.png)
где:	

 - n – степень полинома;
 - 	ai, 0 <= i <= n – коэффициенты полинома (действительные или комплексные числа).

*Полином* можно определить также как выражение из нескольких *термов*, соединенных знаками сложения или вычитания. *Терм* включает коэффициент и *моном*, содержащий одну или несколько переменных, каждая из которых может иметь *степень*
![](http://s019.radikal.ru/i609/1705/9d/61d024f5dabe.png)
Как пример, полином от трех переменных может иметь вид:
![](http://s019.radikal.ru/i628/1705/24/ad8c9e8a076a.png)
*Подобными* называют два (или более) мономов, имеющих одинаковые степени при неизвестных. 

В число основных операций над полиномами входят действия по вычислению значений полинома при заданных значениях переменных, а также большинство известных математических операций (сложение, вычитание, вычисление частных производных, интегрирование и т.п.). 

Полиномы как формальный объект хорошо изучены в математике. Математическая модель данной предметной области – *алгебра полиномов*. 

***
##Требования к лабораторной работе
***

В рамках лабораторной работы ставится задача создания программных средств, поддерживающих эффективное представление полиномов и выполнение следующих операций над ними:

- ввод полинома 
- организация хранения полинома
- удаление введенного ранее полинома;
- копирование полинома;
- сложение двух полиномов;
- вычисление значения полинома при заданных значениях переменных;
- вывод.

Состав реализуемых операций над полиномами может быть расширен при постановке задания лабораторной работы.

Предполагается, что в качестве структуры хранения будут использоваться списки. В качестве дополнительной цели в лабораторной работе ставится также задача разработки некоторого общего представления списков и операций по их обработке. В числе операций над списками должны быть реализованы следующие действия:

-	поддержка понятия текущего звена;
-	вставка звеньев в начало, после текущей позиции и в конец списков;
-	удаление звеньев в начале и в текущей позиции списков;
-	организация последовательного доступа к звеньям списка (итератор).
***
##Условия и ограничения
***
При выполнении лабораторной работы можно использовать следующие основные предположения:

 - Разработка структуры хранения должна быть ориентирована на представление полиномов от трех неизвестных.
 - Степени переменных полиномов не могут превышать значения 9, т.е. ![](http://s018.radikal.ru/i524/1705/56/c6a32b117cf7.png)
 - Число мономов в полиномах существенно меньше максимально возможного количества (тем самым, в структуре хранения должны находиться только мономы с ненулевыми коэффициентами).
 ***
##Методы выполнения лабораторной работы
***
###Структуры хранения полиномов

Для представления полиномов могут быть выбраны различные структуры хранения. Критериями выбора структуры хранения являются размер требуемой памяти и сложность (трудоемкость) реализации операций над полиномами.

Возможный вариант структуры хранения – использование массивов (в случае полиномов от трех переменных – трехмерная матрица коэффициентов полинома). Такой способ обеспечивает простую реализацию операций над полиномами, но он не эффективен в части объема требуемой памяти. Так, при сделанных допущениях для хранения одного полинома в массиве потребуется порядка 8000 байт памяти – при этом в памяти будут храниться в основном параметры мономов с нулевыми коэффициентами.

Разработка более эффективной структуры хранения должна быть выполнена с учетом следующих рекомендаций:

- в структуре хранения должны храниться данные только для мономов с ненулевыми коэффициентами;
- порядок размещения данных в структуре хранения должен обеспечивать возможность быстрого поиска мономов с заданными свойствами (например, для приведения подобных).

Для организации быстрого доступа может быть использовано упорядоченное хранение мономов. Для задания порядка следования можно принять лексикографическое упорядочивание по степеням переменных, при котором мономы упорядочиваются по степеням первой переменной, потом по второй переменной, и только затем по третьей переменной. В общем виде это правило можно записать как соотношение: моном ![](http://s018.radikal.ru/i516/1705/aa/8e74df920fe2.png) предшествует моному ![](http://s019.radikal.ru/i626/1705/ef/56d2e4b03f98.png) тогда и только тогда, если
 ![](http://s018.radikal.ru/i509/1705/b3/2b2b0daddbdb.png)
Проверка лексикографического порядка занимает сравнительно много времени. Ее можно существенно упростить при помощи свернутой степени (индекса) монома, образуемой с использованием позиционной системы счисления: для монома со степенями (A, B, C) ставится в соответствие величина
![](http://s019.radikal.ru/i604/1705/6f/93e13eff4444.png)
Данное соответствие является взаимно-однозначным. Обратное соответствие определяется при помощи выражений
![](http://s019.radikal.ru/i609/1705/15/ed253c9fcf62.png)
Кроме того, введенное соответствие порождает порядок, полностью совпадающий с лексикографическим порядком
![](http://s56.radikal.ru/i154/1705/1e/f29c72d041b6.png)
Выполненное обсуждение позволяет определить, что наиболее эффективным способом организации структуры хранения полиномов являются линейный (односвязный) список. Тем самым, в рамках лабораторной работы появляется подзадача – разработка структуры хранения в виде линейных списков. Данная разработка должна быть выполнена в некоторой общей постановке с тем, чтобы разработанные программы работы со списками могли быть далее использованы и в других ситуациях, в которых необходимы списковые структуры хранения.
2.1.2.	Структуры хранения списков
Возможный вариант общей схемы представления линейных списков может иметь следующий формат:
 ![](http://s014.radikal.ru/i326/1705/33/4ecaa25e7910.png)
где
 - pFirst – указатель на первое звено списка;
 - pLast  – указатель на последнее звено списка;
 - pCurrLink – указатель на текущее звено списка;
 - pPrevLink – указатель на звено, предшествующее текущему;
 - CurrPos – номер текущего звена;
 - ListLen – количество звеньев в списке.

Для повышения общности схемы реализации для фиксации ситуаций, в которых указатель не содержит адрес какого-либо звена списка (например, указатель следующего звена в конце списка) предлагается использовать переменную pStop вместо величины NULL (по умолчанию, эта переменная будут равняться NULL, другие значения константы будут использоваться по необходимости).

Необходимые операции над списком должны определяться при проектировании класса поддержки списков. В число необходимых операций должны входить операции создания и удаления списков, вставки и удаления звеньев, последовательного доступа к звеньям и др.

Применительно к полиномам в поле значений звеньев должны располагаться коэффициент и степень мономов – тем самым, структура хранения для приведенного ранее примера полинома имеет вид:
 ![](http://s016.radikal.ru/i335/1705/ba/31bccfd295b1.png)
При внимательном рассмотрении задачи обработки полиномов может быть рекомендовано определенное расширение общей схемы представления линейных списков:

 - Для обеспечения однородности представления полиномов (в частности, для представления нулевого полинома) целесообразно ввести служебное начальное звено (звено-заголовок):
 ![](http://s018.radikal.ru/i500/1705/a8/ef0b9e76990b.png)
(звено-заголовок маркируется логически-недопустимыми значениями коэффициента и индекса монома).
 - Для повышения эффективности операций обработки полиномов (например, чтобы уйти от проверки нулевого указателя последнего звена списка) целесообразно использовать циклические списки, в которых указатель последнего звена указывает на первое звено:  
 ![](http://s018.radikal.ru/i518/1705/fe/77bd58b293f2.png)

***
##Алгоритмы
***

###Списки
Для работы со списками предлагается реализовать следующие операции:

- методы получения параметров состояния списка (проверка на пустоту, получение текущего количества звеньев);
- метод доступа к значению первого, текущего или последнего звена;
- методы навигации по списку (итератор);
- методы вставки перед первым, после текущего и последнего звеньев;
- методы удаления первого и текущего звена.

P.S. Состав операций может быть расширен при разработке спецификации класса для списков. Расширение списковой структуры хранения для представления полиномов (звено-заголовок, цикличная структура списка) должно разрабатываться как производный класс от базового класса поддержки списков.

###Полиномы

Для работы с полиномами предлагается реализовать следующие операции:

- конструкторы инициализации и копирования;
- метод присваивания;
- метод сложения полиномов.
- Дополнительные операции могут быть определены при разработке спецификации класса для полиномов.

***
##Разработка программного комплекса
***

###Структура проекта
С учетом всех перечисленных ранее требований может быть предложен следующий состав классов и отношения между этими классами:

- класс TDatValue для определения класса объектов-значений списка (абстрактный класс);
- класс TMonom для определения объектов-значений параметров монома;
- класс TRootLink для определения звеньев (элементов) списка (абстрактный класс);
- класс TDatLink для определения звеньев (элементов) списка с указателем на объект-значение;
- класс TDatList для определения линейных списков;
- класс THeadRing для определения циклических списков с заголовком;
- класс TPolinom для определения полиномов.

![](http://s016.radikal.ru/i335/1705/a3/ca36eb00b8b9.png)

На рисунке показаны также отношения между классами: обычными стрелками показаны отношения наследования (базовый класс – производный класс), а ромбовидными стрелками – отношения ассоциации (класс-владелец – класс-компонент).

В соответствии с предложенной структурой классов модульная (файловая) структура программной системы может иметь вид:

- DatValue.h – модуль, объявляющий абстрактный класс объектов-значений списка;
- RootLink.h, RootLink.cpp – модуль базового класса для звеньев (элементов) списка;
- DatLink.h, DatLink.cpp – модуль класса для звеньев (элементов) списка с указателем на объект-значение;
- DatList.h, DatList.cpp – модуль класса линейных списков;
- HeadRing.h, HeadRing.cpp – модуль класса циклических списков с заголовком;
- Monom.h, Monom.cpp – модуль класса моном;
- Polinom.h, Polinom.cpp – модуль класса полиномов;
- PolinomTestkit.cpp – модуль программы тестирования.

Для развития навыков практического программирования рекомендуются следующие направления расширения постановки задачи в реализации дополнительных операций над полиномами 

- вычисление значения 
- сравнение
- и т.д.

При расширении разработанных программ следует обратить внимание на трудоемкость необходимой модификации существующего программного кода: количество методов, в которые требуется внести изменения; количество новых внесенных ошибок разработки и т.п. 

### Цели работы:   


##### В рамках лабораторной работы ставится задача разработки такой структуры данных как **Списки** основанной на структуре хранения *односвязные списки построенные на массиве*;
 
С помощью разработанной структуры **Списки** необходимо написать приложение приложение для работы с полиномами.
***


### Задачи:  
1. Разработка интерфейса всех необходимых классов перечисленных в пункте **Структура проекта**
2. Реализация методов всех необходимых классов согласно заданному интерфейсу.(см. пункт **Структура проекта**)
3. Разработка тестов для проверки работоспособности списков.
4. Реализация алгоритма работы с полиномами.
5. Реализация операций для работы с полиномами.
6. Обеспечение работоспособности тестов и примера использования.
***

### Используемые инструменты:

***
- Система контроля версий Git. 
- Фреймворк для написания автоматических тестов Google Test.
- Среда разработки Microsoft Visual Studio (2017).
***


## Начало работы:

***
### 1. Разработка и реализация класса `TDatacom`:
***

#### Объявление и реализация (h-файл):
```
#ifndef __DATACOM_H__
#define __DATACOM_H__

#define DataOK   0
#define DataErr -1

class TDataCom
{
protected:
  int RetCode; 

  int SetRetCode(int ret) { return RetCode = ret; }
public:
  TDataCom(): RetCode(DataOK) {}
  virtual ~TDataCom() = 0 {}
  int GetRetCode()
  {
    int temp = RetCode;
    RetCode = DataOK;
    return temp;
  }
};

#endif
```
***
### 2. Разработка и реализация абстрактного класса объектов-значений `TDatValue`:
***
#### Объявление и реализация (h-файл):
```
// DatValue.h
// модуль, объявляющий абстрактный класс объектов-значений списка

#ifndef _DATVALUE_H_
#define _DATVALUE_H_

class TDatValue;
typedef TDatValue *PTDatValue;

class TDatValue {
public:
	virtual TDatValue * GetCopy() = 0; // создание копии
	~TDatValue() {}
};
#endif
```

***
### 3. Разработка и реализация класса `TMonom`:
***
#### Объявление и реализация (h-файл):
```
// Monom.h
// модуль класса моном

#ifndef _MONOM_H_
#define _MONOM_H_

#include "DatValue.h"

class TMonom;
typedef TMonom *PTMonom;

class TMonom : public TDatValue {
protected:
	int Coeff; // коэффициент монома
	int Index; // индекс (свертка степеней)
public:
	TMonom(int cval = 1, int ival = 0) {
		Coeff = cval; Index = ival;
	};
	void SetCoeff(int cval) { Coeff = cval; }
	int  GetCoeff(void) { return Coeff; }
	void SetIndex(int ival) { Index = ival; }
	int  GetIndex(void) { return Index; }
	virtual TDatValue * GetCopy() // изготовить копию
	{
		TDatValue * temp = new TMonom(Coeff, Index);
		return temp;
	}
	TMonom& operator=(const TMonom &tm) 
	{
		Coeff = tm.Coeff; Index = tm.Index;
		return *this;
	}
	int operator==(const TMonom &tm) 
	{
		return (Coeff == tm.Coeff) && (Index == tm.Index);
	}
	int operator<(const TMonom &tm) 
	{
			return Index < tm.Index;
	}
	int operator>(const TMonom &tm)
	{
		if (Index != tm.Index)
			return Index > tm.Index;
		else
		{
			return Coeff > tm.Coeff;
		}
	}
	friend class TPolinom;
};
#endif
```

***
### 4. Разработка и реализация базового класса для звеньев (элементов) списка `TRootLink` :
***
#### Объявление и реализация (h-файл):
```
// RootLink.h
// модуль базового класса для звеньев(элементов) списка

#ifndef _ROOTLINK_H_
#define _ROOTLINK_H_

#include "DatValue.h"

class TRootLink;
typedef TRootLink *PTRootLink;

class TRootLink {
protected:
	PTRootLink  pNext;  // указатель на следующее звено
public:
	TRootLink(PTRootLink pN = nullptr) { pNext = pN; }
	PTRootLink  GetNextLink() { return  pNext; }
	void SetNextLink(PTRootLink  pLink) { pNext = pLink; }
	void InsNextLink(PTRootLink  pLink) // вставка звена
	{ 
		PTRootLink p = pNext;  pNext = pLink;
		if (pLink != nullptr) pLink->pNext = p;
	}
	virtual void SetDatValue(PTDatValue pVal) = 0;
	virtual PTDatValue GetDatValue() = 0;

	friend class TDatList;
};
#endif
```

***
### 5. Разработка и реализация класса для звеньев (элементов) списка с указателем на объект-значение `TDatLink`:
***
#### Объявление и реализация (h-файл):
```
// DatLink.h
// модуль класса для звеньев(элементов) списка с указателем на объект - значение

#ifndef _DATLINK_H_
#define _DATLINK_H_

#include "RootLink.h"

class TDatLink;
typedef TDatLink *PTDatLink;

class TDatLink : public TRootLink 
{
protected:
	PTDatValue pValue;  // указатель на объект-значение
public:
	TDatLink(PTDatValue pVal = nullptr, PTRootLink pN = nullptr) : TRootLink(pN)
	{
		pValue = pVal;
	}
	void SetDatValue(PTDatValue pVal) { pValue = pVal; }
	PTDatValue GetDatValue() { return  pValue; }
	PTDatLink  GetNextDatLink() { return  (PTDatLink)pNext; }
	friend class TDatList;
};
#endif
```

***
### 6. Разработка и реализация класса линейных списков `TDatList`:
***

#### Объявление (h-файл):
```
// DatList.h
// модуль класса линейных списков

#ifndef _DATLIST_H_
#define _DATLIST_H_

#include "tdatacom.h"
#include "DatLink.h"

#define ListOK 0 // ошибок нет
#define ListEmpty -101 // список пуст
#define ListNoMem -102 // нет памяти
#define ListNoPos -103 // ошибочное положение текущего указателя

enum TLinkPos { FIRST, CURRENT, LAST };

class DatList;
typedef TDatList *PTDatList;

class TDatList : public TDataCom {
protected:
	PTDatLink pFirst;    // первое звено
	PTDatLink pLast;     // последнее звено
	PTDatLink pCurrLink; // текущее звено
	PTDatLink pPrevLink; // звено перед текущим
	PTDatLink pStop;     // значение указателя, означающего конец списка 
	int CurrPos;         // номер текущего звена (нумерация от 0)
	int ListLen;         // количество звеньев в списке
protected:  // методы
	PTDatLink GetLink(PTDatValue pVal = nullptr, PTDatLink pLink = nullptr);
	void      DelLink(PTDatLink pLink);   // удаление звена
public:
	TDatList();
	~TDatList() { DelList(); }

	// доступ
	PTDatValue GetDatValue(TLinkPos mode = CURRENT) const; // значение
	virtual int IsEmpty()  const { return pFirst == pStop; } // проверка пустоты
	int GetListLength()    const { return ListLen; }       // количество звеньев

														   // навигация
	int SetCurrentPos(int pos);          // установить текущее звено
	int GetCurrentPos(void) const;       // получить номер тек. звена
	virtual int Reset(void);             // установить текущим первое звено
	virtual bool IsListEnded(void) const; // список завершен ?
	int GoNext(void);                    // сдвиг вправо текущего звена
										 // (=1 после применения GoNext для последнего звена списка)

										 // вставка звеньев
	virtual void InsFirst(PTDatValue pVal = nullptr); // перед первым
	virtual void InsLast(PTDatValue pVal = nullptr); // вставить последним 
	virtual void InsCurrent(PTDatValue pVal = nullptr); // перед текущим 

														// удаление звеньев
	virtual void DelFirst(void);    // удалить первое звено 
	virtual void DelCurrent(void);    // удалить текущее звено 
	virtual void DelList(void);    // удалить весь список
};

#endif
```
#### Реализация (cpp-файл):
```
// DatList.cpp
// модуль класса линейных списков

#include "DatList.h"

TDatList::TDatList()
{
	pFirst = pLast = pStop = nullptr;
	ListLen = 0;
}

PTDatLink TDatList::GetLink(PTDatValue pVal, PTDatLink pLink)
{
	PTDatLink temp = new TDatLink(pVal, pLink); // выделение звена
	if (temp == nullptr)
		SetRetCode(ListNoMem);
	else
		SetRetCode(ListOK);
	return temp;
}

void TDatList::DelLink(PTDatLink pLink) // удаление звена
{
	if (pLink != nullptr)
	{
		if (pLink->pValue != nullptr)
			delete pLink->pValue;
		delete pLink;
	}
	SetRetCode(ListOK);
}

PTDatValue TDatList::GetDatValue(TLinkPos mode) const //получить данные звена
{
	PTDatLink temp;
	switch (mode)
	{
	case FIRST: temp = pFirst; break;
	case LAST: temp = pLast; break;
	default: temp = pCurrLink;  break;
	}
	return (temp == nullptr) ? nullptr : temp->pValue;
}

// методы навигаии

int TDatList::SetCurrentPos(int pos) // установить текущее звено
{
	Reset(); // возвращаемся в начало списка
	for (int i = 0; i < pos; i++, GoNext())
		SetRetCode(ListOK);
	return RetCode;
}

int TDatList::GetCurrentPos(void) const // получить номер текущего звена
{
	return CurrPos;
}

/*-------------------------------------------*/

int TDatList::Reset(void) // установить текущим первое звено
{
	pPrevLink = pStop;
	if (IsEmpty())
	{
		pCurrLink = pStop;
		CurrPos = -1;
		SetRetCode(ListEmpty);
	}
	else
	{
		pCurrLink = pFirst;
		CurrPos = 0;
		SetRetCode(ListOK);
	}
	return RetCode;
}

int TDatList::GoNext(void) // сдвиг текущего звена вправо
{
	if (pCurrLink == pStop)
		SetRetCode(ListNoPos); // следующее звено отсутствует
	else
	{
		SetRetCode(ListOK);
		pPrevLink = pCurrLink;
		pCurrLink = pCurrLink->GetNextDatLink();
		CurrPos++;
	}
	return RetCode;
}

bool TDatList::IsListEnded(void) const // список завершен?
{
	// (== true после применения GoNext для последнего звена списка)
	return pCurrLink == pStop;
}

// методы вставки звеньев

void TDatList::InsFirst(PTDatValue pVal) // вставить перед первым
{
	PTDatLink temp = GetLink(pVal, pFirst);
	if (temp == nullptr)
		SetRetCode(ListNoMem);
	else
	{
		pFirst = temp; ListLen++;
		// проверка пустоты списка перед вставкой
		if (ListLen == 1)
		{
			pLast = temp;
			Reset();
		}
		// коррекция текущей позиции - отличие обработки для начала списка
		else
		{
			if (CurrPos == 0)
				pCurrLink = temp;
			else
				CurrPos++;
		}
		SetRetCode(ListOK);
	}
}

void TDatList::InsLast(PTDatValue pVal) // вставить последним
{
	PTDatLink temp = GetLink(pVal, pStop);
	if (temp == nullptr)
		SetRetCode(ListNoMem);
	else
	{
		if (pLast != nullptr)
			pLast->SetNextLink(temp);
		pLast = temp; ListLen++;
		// проверка пустоты списка перед вставкой
		if (ListLen == 1)
		{
			pFirst = temp;
			Reset();
		}
		// корректировка текущей позиции - отличие при CurrLink за концом списка
		if (IsListEnded())
			pCurrLink = temp;
		SetRetCode(ListOK);
	}
}

void TDatList::InsCurrent(PTDatValue pVal) // вставить перед текущим
{
	if (IsEmpty() || (pCurrLink == pFirst))
		InsFirst(pVal);
	else if (IsListEnded())
		InsLast(pVal);
	else if (pPrevLink == pStop)
		SetRetCode(ListNoMem);
	else
	{
		PTDatLink temp = GetLink(pVal, pCurrLink);
		if (temp == nullptr)
			SetRetCode(ListNoMem);
		else
		{
			pCurrLink = temp; pPrevLink->SetNextLink(temp);
			ListLen++; SetRetCode(ListOK);
		}
	}
}

// методы удаления звеньев

void TDatList::DelFirst(void) // удалить первое звено
{
	if (IsEmpty())
		SetRetCode(ListEmpty);
	else
	{
		PTDatLink temp = pFirst;
		pFirst = pFirst->GetNextDatLink();
		DelLink(temp); ListLen--;
		if (IsEmpty())
		{
			pLast = pStop;
			Reset();
		}
		// корректировка текущей позиции - отличие обработки для начала списка
		else if (CurrPos == 0)
			pCurrLink = pFirst;
		else if (CurrPos == 1)
			pPrevLink = pStop;
		if (CurrPos > 0)
			CurrPos--;
		SetRetCode(ListOK);
	}
}

void TDatList::DelCurrent(void) // удалить текущее звено
{
	if (pCurrLink == pStop)
		SetRetCode(ListNoPos);
	else if (pCurrLink == pFirst)
		DelFirst();
	else
	{
		PTDatLink temp = pCurrLink;
		pCurrLink = pCurrLink->GetNextDatLink();
		pPrevLink->SetNextLink(pCurrLink);
		DelLink(temp); ListLen--;
		// обработка ситуации удаления последнего звена
		if (pCurrLink == pLast)
		{
			pLast = pPrevLink;
			pCurrLink = pStop;
		}
		SetRetCode(ListOK);
	}
}

void TDatList::DelList(void) // удалить весь список
{
	while (!IsEmpty())
		DelFirst();
	pFirst = pLast = pPrevLink = pCurrLink = pStop;
	CurrPos = -1;
}
```

***
### 7. Разработка и реализация класса циклических списков с заголовком `THeadRing`:
***
#### Объявление (h-файл):
```
// HeadRing.h
// модуль класса циклических списков с заголовком

#ifndef _HEADRING_H_
#define _HEADRING_H_

#include "DatList.h"

class THeadRing : public TDatList {
protected:
	PTDatLink pHead;     // заголовок, pFirst - будет следующим за pHead
public:
	THeadRing();
	~THeadRing();
	// вставка звеньев
	virtual void InsFirst(PTDatValue pVal = nullptr); // после заголовка
													  // удаление звеньев
	virtual void DelFirst(void);                 // удалить первое звено
};
#endif
```
#### Реализация (cpp-файл):
```
// HeadRing.cpp
// модуль класса циклических списков с заголовком

#include "HeadRing.h"

THeadRing::THeadRing() : TDatList()
{
	InsLast();
	pHead = pFirst;
	ListLen = 0;
	pStop = pHead;
	Reset();
	pFirst->SetNextLink(pFirst);
}

THeadRing::~THeadRing()
{
	TDatList::~TDatList();
	DelLink(pHead);
	pHead = nullptr;
}

void THeadRing::InsFirst(PTDatValue pVal) // вставить после заголовка
{
	TDatList::InsFirst(pVal);
	if (RetCode == DataOK)
		pHead->SetNextLink(pFirst);
}

void THeadRing::DelFirst(void) // удалить первое звено
{
	TDatList::DelFirst();
	pHead->SetNextLink(pFirst);
}
```

***
### 8. Разработка и реализация класса полиномов `TPolinom`:
***

#### Объявление (h-файл):
```
// Polinom.h
// модуль класса полиномов

#ifndef _POLINOM_H_
#define _POLINOM_H_

#include <iostream>
#include "HeadRing.h"
#include "Monom.h"

using namespace std;

class TPolinom : public THeadRing {
private:
	long GetPow(long val, int power);
public:
	TPolinom(int monoms[][2] = nullptr, int km = 0); // конструктор полинома 
													 // из массива «коэффициент-индекс»
	TPolinom(TPolinom &q); // конструктор копирования
	PTMonom  GetMonom() { return (PTMonom)GetDatValue(); }
	TPolinom & operator+(TPolinom &q); // сложение полиномов
	long CalculatePoly(int x, int y, int z); // вычислить значение полинома 
	bool operator==(TPolinom &q); // сравнение полиномов
	bool operator!=(TPolinom &q); // сравнение полиномов
	TPolinom & operator=(TPolinom &q); // присваивание
	friend ostream& operator<<(ostream &os, TPolinom &q); // печать полинома
	friend istream& operator>>(istream &is, TPolinom &q); // ввод полинома
};
#endif
```
#### Реализация (cpp-файл):
```
// Polinom.cpp
// модуль класса полиномов

#include "Polinom.h"

TPolinom::TPolinom(int monoms[][2], int km)
{
	PTMonom pMonom = new TMonom(0, -1);
	pHead->SetDatValue(pMonom);
	for (int i = 0; i < km; i++)
	{
		pMonom = new TMonom(monoms[i][0], monoms[i][1]);
		InsLast(pMonom);
	}
}

/*-------------------------------------------*/

TPolinom & TPolinom::operator+(TPolinom &q) // сложение полиномов
{
	PTMonom pm, qm, tm;
	Reset(); q.Reset();
	while (true)
	{
		pm = GetMonom(); qm = q.GetMonom();
		if (pm->Index < qm->Index)
		{
			// степени монома pm меньше степеней монома qm 
			// >> добавление монома qm  в полином p
			tm = new TMonom(qm->Coeff, qm->Index);
			InsCurrent(tm); q.GoNext();
		}
		else if (pm->Index > qm->Index)
			GoNext();
		else // индексы мономов равны
		{
			if (pm->Index == -1) // звенья не должны быть заголовочными
				break;
			pm->Coeff += qm->Coeff;
			if (pm->Coeff != 0)
			{
				GoNext(); q.GoNext();
			}
			else // удаление монома с нулевым коэффициентом
			{
				DelCurrent(); q.GoNext();
			}
		}
	}
	return *this;
}

/*-------------------------------------------*/

long TPolinom::GetPow(long val, int power)
{
	long result = 1;
	for (int i = 0; i < power; i++)
		result *= val;
	return result;
}

/*-------------------------------------------*/

long TPolinom::CalculatePoly(int x, int y, int z) // вычислить значение полинома 
{
	long result = 0;
	if (GetListLength() != 0)
	{
		PTMonom pMonom;
		int cf, index;
		for (Reset(); !IsListEnded(); GoNext())
		{
			pMonom = GetMonom();
			cf = pMonom->GetCoeff();
			index = pMonom->GetIndex();
			result += cf * GetPow(x, index / 100) * GetPow(y, (index % 100) / 10) * GetPow(z, index % 10);
		}
	}
	return result;
}

/*-------------------------------------------*/

bool TPolinom::operator==(TPolinom &q) // сравнение полиномов
{
	if (GetListLength() != q.GetListLength())
		return false;
	else
	{
		PTMonom pMon, qMon;
		Reset(); q.Reset();
		while (!IsListEnded())
		{
			pMon = GetMonom();
			qMon = q.GetMonom();
			if (*pMon == *qMon)
			{
				GoNext(); q.GoNext();
			}
			else
				return false;
		}
		return true;
	}
}

/*-------------------------------------------*/

bool TPolinom::operator!=(TPolinom &q) // сравнение полиномов
{
	return !(*this == q);
}
/*-------------------------------------------*/

TPolinom::TPolinom(TPolinom &q) // конструктор копирования
{
	PTMonom pMonom = new TMonom(0, -1);
	pHead->SetDatValue(pMonom);
	for (q.Reset(); !q.IsListEnded(); q.GoNext())
	{
		pMonom = q.GetMonom();
		InsLast(pMonom->GetCopy());
	}
	q.Reset();
}

/*-------------------------------------------*/

TPolinom & TPolinom::operator=(TPolinom &q) // присваивание
{
	if (this != &q)
	{
		PTMonom pMonom;
		DelList();
		for (q.Reset(); !q.IsListEnded(); q.GoNext())
		{
			pMonom = q.GetMonom();
			InsLast(pMonom->GetCopy());
		}
		q.Reset();
	}
	return *this;
}

/*-------------------------------------------*/

ostream& operator<<(ostream &os, TPolinom &q)
{
	PTMonom pMonom;
	int cf, x, y, z;
	if (q.GetListLength() > 0)
	{
		for (q.Reset(); !q.IsListEnded(); q.GoNext())
		{
			pMonom = q.GetMonom();
			cf = pMonom->GetCoeff();
			x = pMonom->GetIndex();
			y = (x % 100) / 10;
			z = x % 10;
			x = x / 100;
			if (cf != 0)
			{
				if ((cf > 0) && (q.GetCurrentPos() != 0))
					os << "+" << cf;
				else
					os << cf;
				if (x > 0)
					os << "x^" << x;
				if (y > 0)
					os << "y^" << y;
				if (z > 0)
					os << "z^" << z;
				os << " ";
			}
		}
	}
	else
		os << 0;
	q.Reset();
	return os;
}

/*-------------------------------------------*/

istream& operator>>(istream &is, TPolinom &q)
{
	if (q.GetListLength() > 0)
		q.DelList();
	char polystr[256];
	char mnstr[64];
	PTMonom tempMonom;
	int pos = 0, i, cf, x, y, z;
	is.getline(polystr, 256);
	q.Reset();
	while (polystr[pos])
	{
		i = 0;
		while ((polystr[pos] != ' ') && (polystr[pos]))
		{
			if (polystr[pos] != '+')
			{
				mnstr[i] = polystr[pos];
				i++;
			}
			pos++;
		}
		mnstr[i] = 0;
		i = 0;
		if (mnstr[0] == '-')
			i++;
		cf = x = y = z = 0;
		while ((mnstr[i] != 'x') && (mnstr[i] != 'y') && (mnstr[i] != 'z') && (mnstr[i] != 0))
		{
			cf = cf * 10 + (mnstr[i] - '0');
			i++;
		}
		if (mnstr[0] == '-')
			cf *= -1;
		if (mnstr[i] == 'x')
		{
			i += 2;
			x = mnstr[i] - '0';
		}
		while ((mnstr[i] != 'y') && (mnstr[i] != 'z') && (mnstr[i] != 0))
			i++;
		if (mnstr[i] == 'y')
		{
			i += 2;
			y = mnstr[i] - '0';
		}
		while ((mnstr[i] != 'z') && (mnstr[i] != 0))
			i++;
		if (mnstr[i] == 'z')
		{
			i += 2;
			z = mnstr[i] - '0';
		}
		tempMonom = new TMonom(cf, x * 100 + y * 10 + z);
		q.InsLast(tempMonom);
		if (polystr[pos] != 0)
			pos++;
		mnstr[0] = 0;
	}
	q.Reset();
	return is;
}
```

***
### 9. Разработка и реализация тестовой программы `PolinomTestkit`:
***
#### Реализация (cpp-файл):
```
// poly_testkit.cpp
// пример испозьвания класса TPolinom

#include "Polinom.h"

int main()
{
	setlocale(LC_ALL, "Russian");
	cout << "Тестирование полиномов" << endl;

	TPolinom poly1, poly2, temppol;
	int x, y, z;

	cout << "Введите полиномы: " << endl;
	cin >> poly1;
	cin >> poly2;
	if (poly1 == poly2)
		cout << "Полиномы равны" << endl;
	else
		cout << "Полиномы не равны" << endl;
	temppol = poly1;
	temppol + poly2;
	cout << poly1 << " + " << poly2 << " = " << endl;
	cout << temppol << endl;
	cout << "Полином №1 " << poly1 <<endl;
	cout << "Полином №2 " << poly2 << endl;
	cout << "Вычислить значение суммы при: ";
	cin >> x >> y >> z; 
	cout << endl;
	cout << "Результат: " << temppol.CalculatePoly(x, y, z) << endl;
	return 0;
}
```
***
###10. Тесты для проверки  **TMonom** и **TDatList** (GoogleTestFramework)
***
#####**Test_gtest.cpp**
```c++
#include <stdio.h>
#include <gtest/gtest.h>
#include "PolyGMU\DatList.h"
#include "PolyGMU\DatList.cpp"
#include "PolyGMU\Monom.h"


TEST(TMonom, can_create_monom)
{
	ASSERT_NO_THROW(TMonom monom_1);
}

TEST(TMonom, can_create_monom_with_args)
{
	ASSERT_NO_THROW(TMonom monom_1(-18, 253));
}

TEST(TMonom, can_set_monom_coeff)
{
	TMonom monom_1;
	ASSERT_NO_THROW(monom_1.SetCoeff(15));
}

TEST(TMonom, can_get_monom_coeff)
{
	TMonom monom_1(16, 1);
	EXPECT_EQ(16, monom_1.GetCoeff());
}

TEST(TMonom, can_set_monom_index)
{
	TMonom monom_1;
	ASSERT_NO_THROW(monom_1.SetIndex(18));
}

TEST(TMonom, can_get_monom_index)
{
	TMonom monom_1(1, 3);
	EXPECT_EQ(3, monom_1.GetIndex());
}

TEST(TMonom, can_assign_monoms)
{
	TMonom monom_1(10, 3), monom_2(-10, 6);
	monom_2 = monom_1;
	EXPECT_TRUE((monom_1.GetCoeff() == monom_2.GetCoeff()) && (monom_1.GetIndex() == monom_2.GetIndex()));
}

TEST(TMonom, can_get_copy)
{
	TMonom monom_1(10, 243);
	TMonom * monom_2;
	monom_2 = (TMonom *)monom_1.GetCopy();
	EXPECT_TRUE((monom_1.GetCoeff() == monom_2->GetCoeff()) && (monom_1.GetIndex() == monom_2->GetIndex()));
}

TEST(TMonom, equal_monoms_are_equal)
{
	TMonom monom_1(15, 235), monom_2(15, 235);
	EXPECT_TRUE(monom_1 == monom_2);
}

TEST(TMonom, monom_with_less_index_is_less)
{
	TMonom monom_1(54, 81), monom_2(4, 135);
	EXPECT_TRUE(monom_1 < monom_2);
}

TEST(TMonom, monom_with_less_coeff_is_less)
{
	TMonom monom_1(54, 81), monom_2(4, 81);
	EXPECT_TRUE(monom_1 > monom_2);
}

/*---------------------*/


TEST(TDatList, can_create_list)
{
	ASSERT_NO_THROW(TDatList list_1);
}

TEST(TDatList, new_list_is_empty)
{
	TDatList list_1;
	ASSERT_TRUE(list_1.IsEmpty());
}

TEST(TDatList, can_put_link_in_list)
{
	TDatList list_1;
	PTMonom monom_1;
	monom_1 = new TMonom(4, 123);
	ASSERT_NO_THROW(list_1.InsLast(monom_1));
}

TEST(TDatList, list_with_links_is_not_empty)
{
	TDatList list_1;
	PTMonom monom_1, monom_2;
	monom_1 = new TMonom(4, 123);
	monom_2 = new TMonom(8, 456);
	list_1.InsLast(monom_1);
	list_1.InsLast(monom_2);
	ASSERT_TRUE((!list_1.IsEmpty()) && (list_1.GetListLength() == 2));
}

TEST(TDatList, can_get_current_position)
{
	TDatList list_1;
	PTMonom pVal;
	for (int i = 1; i < 7; i++)
	{
		pVal = new TMonom(i+4, i * 100 + (i+1) * 10 + i+3);
		list_1.InsLast(pVal);
	}
	EXPECT_EQ(list_1.GetCurrentPos(), 0);
}

TEST(TDatList, can_set_current_position)
{
	TDatList list_1;
	PTMonom pVal;
	for (int i = 1; i < 7; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		list_1.InsLast(pVal);
	}
	list_1.SetCurrentPos(3);
	EXPECT_EQ(list_1.GetCurrentPos(), 3);
}

TEST(TDatList, can_go_next_link)
{
	TDatList list_1;
	PTMonom pVal;
	for (int i = 1; i < 7; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		list_1.InsLast(pVal);
	}
	list_1.SetCurrentPos(2);
	list_1.GoNext();
	EXPECT_EQ(list_1.GetCurrentPos(), 3);
}

TEST(TDatList, can_reset_position)
{
	TDatList list_1;
	PTMonom pVal;
	for (int i = 1; i < 7; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		list_1.InsLast(pVal);
	}
	list_1.SetCurrentPos(3);
	list_1.Reset();
	EXPECT_EQ(list_1.GetCurrentPos(), 0);
}

TEST(TDatList, ended_list_is_ended)
{
	TDatList list_1;
	PTMonom pVal;
	for (int i = 1; i < 7; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		list_1.InsLast(pVal);
	}
	list_1.SetCurrentPos(4);
	list_1.GoNext();
	list_1.GoNext();
	EXPECT_TRUE(list_1.IsListEnded());
}

TEST(TDatList, can_get_link_from_list)
{
	TDatList list_1;
	PTMonom pVal;
	for (int i = 1; i < 7; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		list_1.InsLast(pVal);
	}
	list_1.SetCurrentPos(4);
	pVal = (PTMonom)list_1.GetDatValue();
	EXPECT_EQ(pVal->GetIndex() + pVal->GetCoeff(), 560);
}

TEST(TDatList, can_put_link_before_the_first)
{
	TDatList list_1;
	PTMonom pVal, temp;
	for (int i = 1; i < 7; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		list_1.InsLast(pVal);
	}
	pVal = new TMonom(6, 666);
	list_1.InsFirst(pVal);
	temp = (PTMonom)list_1.GetDatValue();
	EXPECT_EQ(temp->GetIndex() + temp->GetCoeff(), 672);
}

TEST(TDatList, can_put_link_after_the_last)
{
	TDatList l1;
	PTMonom pVal, temp;
	for (int i = 0; i < 5; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	pVal = new TMonom(6, 666);
	l1.InsLast(pVal);
	l1.SetCurrentPos(5);
	temp = (PTMonom)l1.GetDatValue();
	EXPECT_EQ(temp->GetIndex() + temp->GetCoeff(), 672);
}

TEST(TDatList, can_put_link_before_the_current)
{
	TDatList l1;
	PTMonom pVal, temp;
	for (int i = 0; i < 5; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	l1.SetCurrentPos(2);
	pVal = new TMonom(6, 666);
	l1.InsCurrent(pVal);
	temp = (PTMonom)l1.GetDatValue();
	EXPECT_EQ(temp->GetIndex() + temp->GetCoeff(), 672);
}

TEST(TDatList, can_delete_first_link)
{
	TDatList l1;
	PTMonom pVal, temp;
	for (int i = 0; i < 5; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	temp = (PTMonom)l1.GetDatValue();
	l1.DelFirst();
	pVal = (PTMonom)l1.GetDatValue();
	EXPECT_TRUE((temp->GetIndex() + temp->GetCoeff() != pVal->GetIndex() + pVal->GetCoeff()) && (l1.GetListLength() == 4));
}

TEST(TDatList, can_delete_current_link)
{
	TDatList l1;
	PTMonom pVal;
	for (int i = 0; i < 5; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	l1.SetCurrentPos(3);
	l1.DelCurrent();
	l1.SetCurrentPos(3);
	pVal = (PTMonom)l1.GetDatValue();
	EXPECT_TRUE((pVal->GetIndex() + pVal->GetCoeff() == 448) && (l1.GetListLength() == 4));
}

TEST(TDatList, can_delete_list)
{
	TDatList l1;
	PTMonom pVal;
	for (int i = 0; i < 5; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	l1.DelList();
	EXPECT_TRUE((l1.IsEmpty()) && (l1.GetListLength() == 0) && (l1.GetDatValue() == nullptr));
}

TEST(TDatList, can_carry_out_multiple_operations)
{
	TDatList list_1;
	PTMonom pVal;
	int temp, length;
	for (int i = 1; i < 7; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		list_1.InsLast(pVal);
	}
	length = list_1.GetListLength();
	list_1.SetCurrentPos(4);
	list_1.GoNext();
	list_1.GoNext();
	list_1.SetCurrentPos(length - 4);
	for (int i = 0; i < 5; i++)
	{
		list_1.DelCurrent();
	}
	length = list_1.GetListLength();
	EXPECT_TRUE(length, 1);
}

int main(int argc, char *argv[])
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
```

***
### 11. Обеспечение работоспособности тестов:
***
![](http://s019.radikal.ru/i643/1705/de/ca5aa7b3bca1.jpg)
***
###12. Результат работы программы:
![](http://i013.radikal.ru/1705/85/dffb214a6c7e.png)
***
### 13. Анализ результата
***
Несмотря на то что, результаты программа выдает верно и правильно выполняет поставленные задачи, у данной реализации есть очевидные минусы и естественные возможности для доработки.

- Выбранный шаблон методов можно либо дополнить сложным образом либо заменить. 
- Для таких объектов как полином очень важно представление, которое в данном случае представлен консольным видом(Возможность работы с формами можно было бы реализовать используя более удобный для этого C#)
- Очень малоприменимая прикладная область для данного проекта.   

***
# Вывод:
***
##### Все цели и задачи работы достигнуты. В ходе выполнения данной работы, мне удалось разработать и реализовать несколько классов:

- класс `TDatValue` для определения класса объектов-значений списка (абстрактный класс);
- класс `TMonom` для определения объектов-значений параметров монома;
- класс `TRootLink` для определения звеньев (элементов) списка (абстрактный класс);
- класс `TDatLink` для определения звеньев (элементов) списка с указателем на объект-значение;
- класс `TDatList` для определения линейных списков;
- класс `THeadRing`для определения циклических списков с заголовком;
- класс `TPolinom` для определения полиномов.

##### Также  мной были написаны тесты данному проекту под систему тестирования `Google Test`, что позволяет не только проверить правильность работы программы, но и также лучше ознакомиться с `Google Test Framework`. Успешное же прохождение всех тестов показывает, что классы были реализованы корректно и программа созданная с использованием этих классов работает верно. 
##### Особенностью данной работы было то, что:
1. Тесты для проверки корректности работы были написаны полностью самостоятельно, и в связи с большим количеством методов работы были сосредоточены на количественной и качественной проверке их правильности и работоспособности.
2. Была освоена структура **"Односвязных списков"**, которая может быть использована в дальнейших работах.
3. Реализация данной структуры была осуществлена изначально без использования предыдущего опыта(за исключением `tdatacom`)
4. Также на основе данной структуры была создана программа для работы с полиномами. 
5.  Данный проект может быть улучшен для возможности выполнения дополнительных действий с классом объектов *полином*.
