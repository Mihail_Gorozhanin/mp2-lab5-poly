﻿// DatList.cpp
// модуль класса линейных списков

#include "DatList.h"

TDatList::TDatList()
{
	pFirst = pLast = pStop = nullptr;
	ListLen = 0;
}

PTDatLink TDatList::GetLink(PTDatValue pVal, PTDatLink pLink)
{
	PTDatLink temp = new TDatLink(pVal, pLink); // выделение звена
	if (temp == nullptr)
		SetRetCode(ListNoMem);
	else
		SetRetCode(ListOK);
	return temp;
}

void TDatList::DelLink(PTDatLink pLink) // удаление звена
{
	if (pLink != nullptr)
	{
		if (pLink->pValue != nullptr)
			delete pLink->pValue;
		delete pLink;
	}
	SetRetCode(ListOK);
}

PTDatValue TDatList::GetDatValue(TLinkPos mode) const //получить данные звена
{
	PTDatLink temp;
	switch (mode)
	{
	case FIRST: temp = pFirst; break;
	case LAST: temp = pLast; break;
	default: temp = pCurrLink;  break;
	}
	return (temp == nullptr) ? nullptr : temp->pValue;
}

// методы навигаии

int TDatList::SetCurrentPos(int pos) // установить текущее звено
{
	Reset(); // возвращаемся в начало списка
	for (int i = 0; i < pos; i++, GoNext())
		SetRetCode(ListOK);
	return RetCode;
}

int TDatList::GetCurrentPos(void) const // получить номер текущего звена
{
	return CurrPos;
}

/*-------------------------------------------*/

int TDatList::Reset(void) // установить текущим первое звено
{
	pPrevLink = pStop;
	if (IsEmpty())
	{
		pCurrLink = pStop;
		CurrPos = -1;
		SetRetCode(ListEmpty);
	}
	else
	{
		pCurrLink = pFirst;
		CurrPos = 0;
		SetRetCode(ListOK);
	}
	return RetCode;
}

int TDatList::GoNext(void) // сдвиг текущего звена вправо
{
	if (pCurrLink == pStop)
		SetRetCode(ListNoPos); // следующее звено отсутствует
	else
	{
		SetRetCode(ListOK);
		pPrevLink = pCurrLink;
		pCurrLink = pCurrLink->GetNextDatLink();
		CurrPos++;
	}
	return RetCode;
}

bool TDatList::IsListEnded(void) const // список завершен?
{
	// (== true после применения GoNext для последнего звена списка)
	return pCurrLink == pStop;
}

// методы вставки звеньев

void TDatList::InsFirst(PTDatValue pVal) // вставить перед первым
{
	PTDatLink temp = GetLink(pVal, pFirst);
	if (temp == nullptr)
		SetRetCode(ListNoMem);
	else
	{
		pFirst = temp; ListLen++;
		// проверка пустоты списка перед вставкой
		if (ListLen == 1)
		{
			pLast = temp;
			Reset();
		}
		// коррекция текущей позиции - отличие обработки для начала списка
		else
		{
			if (CurrPos == 0)
				pCurrLink = temp;
			else
				CurrPos++;
		}
		SetRetCode(ListOK);
	}
}

void TDatList::InsLast(PTDatValue pVal) // вставить последним
{
	PTDatLink temp = GetLink(pVal, pStop);
	if (temp == nullptr)
		SetRetCode(ListNoMem);
	else
	{
		if (pLast != nullptr)
			pLast->SetNextLink(temp);
		pLast = temp; ListLen++;
		// проверка пустоты списка перед вставкой
		if (ListLen == 1)
		{
			pFirst = temp;
			Reset();
		}
		// корректировка текущей позиции - отличие при CurrLink за концом списка
		if (IsListEnded())
			pCurrLink = temp;
		SetRetCode(ListOK);
	}
}

void TDatList::InsCurrent(PTDatValue pVal) // вставить перед текущим
{
	if (IsEmpty() || (pCurrLink == pFirst))
		InsFirst(pVal);
	else if (IsListEnded())
		InsLast(pVal);
	else if (pPrevLink == pStop)
		SetRetCode(ListNoMem);
	else
	{
		PTDatLink temp = GetLink(pVal, pCurrLink);
		if (temp == nullptr)
			SetRetCode(ListNoMem);
		else
		{
			pCurrLink = temp; pPrevLink->SetNextLink(temp);
			ListLen++; SetRetCode(ListOK);
		}
	}
}

// методы удаления звеньев

void TDatList::DelFirst(void) // удалить первое звено
{
	if (IsEmpty())
		SetRetCode(ListEmpty);
	else
	{
		PTDatLink temp = pFirst;
		pFirst = pFirst->GetNextDatLink();
		DelLink(temp); ListLen--;
		if (IsEmpty())
		{
			pLast = pStop;
			Reset();
		}
		// корректировка текущей позиции - отличие обработки для начала списка
		else if (CurrPos == 0)
			pCurrLink = pFirst;
		else if (CurrPos == 1)
			pPrevLink = pStop;
		if (CurrPos > 0)
			CurrPos--;
		SetRetCode(ListOK);
	}
}

void TDatList::DelCurrent(void) // удалить текущее звено
{
	if (pCurrLink == pStop)
		SetRetCode(ListNoPos);
	else if (pCurrLink == pFirst)
		DelFirst();
	else
	{
		PTDatLink temp = pCurrLink;
		pCurrLink = pCurrLink->GetNextDatLink();
		pPrevLink->SetNextLink(pCurrLink);
		DelLink(temp); ListLen--;
		// обработка ситуации удаления последнего звена
		if (pCurrLink == pLast)
		{
			pLast = pPrevLink;
			pCurrLink = pStop;
		}
		SetRetCode(ListOK);
	}
}

void TDatList::DelList(void) // удалить весь список
{
	while (!IsEmpty())
		DelFirst();
	pFirst = pLast = pPrevLink = pCurrLink = pStop;
	CurrPos = -1;
}