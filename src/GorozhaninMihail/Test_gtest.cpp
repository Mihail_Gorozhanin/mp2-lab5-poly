#include <stdio.h>
#include <gtest/gtest.h>
#include "PolyGMU\DatList.h"
#include "PolyGMU\DatList.cpp"
#include "PolyGMU\Monom.h"


TEST(TMonom, can_create_monom)
{
	ASSERT_NO_THROW(TMonom monom_1);
}

TEST(TMonom, can_create_monom_with_args)
{
	ASSERT_NO_THROW(TMonom monom_1(-18, 253));
}

TEST(TMonom, can_set_monom_coeff)
{
	TMonom monom_1;
	ASSERT_NO_THROW(monom_1.SetCoeff(15));
}

TEST(TMonom, can_get_monom_coeff)
{
	TMonom monom_1(16, 1);
	EXPECT_EQ(16, monom_1.GetCoeff());
}

TEST(TMonom, can_set_monom_index)
{
	TMonom monom_1;
	ASSERT_NO_THROW(monom_1.SetIndex(18));
}

TEST(TMonom, can_get_monom_index)
{
	TMonom monom_1(1, 3);
	EXPECT_EQ(3, monom_1.GetIndex());
}

TEST(TMonom, can_assign_monoms)
{
	TMonom monom_1(10, 3), monom_2(-10, 6);
	monom_2 = monom_1;
	EXPECT_TRUE((monom_1.GetCoeff() == monom_2.GetCoeff()) && (monom_1.GetIndex() == monom_2.GetIndex()));
}

TEST(TMonom, can_get_copy)
{
	TMonom monom_1(10, 243);
	TMonom * monom_2;
	monom_2 = (TMonom *)monom_1.GetCopy();
	EXPECT_TRUE((monom_1.GetCoeff() == monom_2->GetCoeff()) && (monom_1.GetIndex() == monom_2->GetIndex()));
}

TEST(TMonom, equal_monoms_are_equal)
{
	TMonom monom_1(15, 235), monom_2(15, 235);
	EXPECT_TRUE(monom_1 == monom_2);
}

TEST(TMonom, monom_with_less_index_is_less)
{
	TMonom monom_1(54, 81), monom_2(4, 135);
	EXPECT_TRUE(monom_1 < monom_2);
}

TEST(TMonom, monom_with_less_coeff_is_less)
{
	TMonom monom_1(54, 81), monom_2(4, 81);
	EXPECT_TRUE(monom_1 > monom_2);
}

/*---------------------*/


TEST(TDatList, can_create_list)
{
	ASSERT_NO_THROW(TDatList list_1);
}

TEST(TDatList, new_list_is_empty)
{
	TDatList list_1;
	ASSERT_TRUE(list_1.IsEmpty());
}

TEST(TDatList, can_put_link_in_list)
{
	TDatList list_1;
	PTMonom monom_1;
	monom_1 = new TMonom(4, 123);
	ASSERT_NO_THROW(list_1.InsLast(monom_1));
}

TEST(TDatList, list_with_links_is_not_empty)
{
	TDatList list_1;
	PTMonom monom_1, monom_2;
	monom_1 = new TMonom(4, 123);
	monom_2 = new TMonom(8, 456);
	list_1.InsLast(monom_1);
	list_1.InsLast(monom_2);
	ASSERT_TRUE((!list_1.IsEmpty()) && (list_1.GetListLength() == 2));
}

TEST(TDatList, can_get_current_position)
{
	TDatList list_1;
	PTMonom pVal;
	for (int i = 1; i < 7; i++)
	{
		pVal = new TMonom(i+4, i * 100 + (i+1) * 10 + i+3);
		list_1.InsLast(pVal);
	}
	EXPECT_EQ(list_1.GetCurrentPos(), 0);
}

TEST(TDatList, can_set_current_position)
{
	TDatList list_1;
	PTMonom pVal;
	for (int i = 1; i < 7; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		list_1.InsLast(pVal);
	}
	list_1.SetCurrentPos(3);
	EXPECT_EQ(list_1.GetCurrentPos(), 3);
}

TEST(TDatList, can_go_next_link)
{
	TDatList list_1;
	PTMonom pVal;
	for (int i = 1; i < 7; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		list_1.InsLast(pVal);
	}
	list_1.SetCurrentPos(2);
	list_1.GoNext();
	EXPECT_EQ(list_1.GetCurrentPos(), 3);
}

TEST(TDatList, can_reset_position)
{
	TDatList list_1;
	PTMonom pVal;
	for (int i = 1; i < 7; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		list_1.InsLast(pVal);
	}
	list_1.SetCurrentPos(3);
	list_1.Reset();
	EXPECT_EQ(list_1.GetCurrentPos(), 0);
}

TEST(TDatList, ended_list_is_ended)
{
	TDatList list_1;
	PTMonom pVal;
	for (int i = 1; i < 7; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		list_1.InsLast(pVal);
	}
	list_1.SetCurrentPos(4);
	list_1.GoNext();
	list_1.GoNext();
	EXPECT_TRUE(list_1.IsListEnded());
}

TEST(TDatList, can_get_link_from_list)
{
	TDatList list_1;
	PTMonom pVal;
	for (int i = 1; i < 7; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		list_1.InsLast(pVal);
	}
	list_1.SetCurrentPos(4);
	pVal = (PTMonom)list_1.GetDatValue();
	EXPECT_EQ(pVal->GetIndex() + pVal->GetCoeff(), 560);
}

TEST(TDatList, can_put_link_before_the_first)
{
	TDatList list_1;
	PTMonom pVal, temp;
	for (int i = 1; i < 7; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		list_1.InsLast(pVal);
	}
	pVal = new TMonom(6, 666);
	list_1.InsFirst(pVal);
	temp = (PTMonom)list_1.GetDatValue();
	EXPECT_EQ(temp->GetIndex() + temp->GetCoeff(), 672);
}

TEST(TDatList, can_put_link_after_the_last)
{
	TDatList l1;
	PTMonom pVal, temp;
	for (int i = 0; i < 5; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	pVal = new TMonom(6, 666);
	l1.InsLast(pVal);
	l1.SetCurrentPos(5);
	temp = (PTMonom)l1.GetDatValue();
	EXPECT_EQ(temp->GetIndex() + temp->GetCoeff(), 672);
}

TEST(TDatList, can_put_link_before_the_current)
{
	TDatList l1;
	PTMonom pVal, temp;
	for (int i = 0; i < 5; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	l1.SetCurrentPos(2);
	pVal = new TMonom(6, 666);
	l1.InsCurrent(pVal);
	temp = (PTMonom)l1.GetDatValue();
	EXPECT_EQ(temp->GetIndex() + temp->GetCoeff(), 672);
}

TEST(TDatList, can_delete_first_link)
{
	TDatList l1;
	PTMonom pVal, temp;
	for (int i = 0; i < 5; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	temp = (PTMonom)l1.GetDatValue();
	l1.DelFirst();
	pVal = (PTMonom)l1.GetDatValue();
	EXPECT_TRUE((temp->GetIndex() + temp->GetCoeff() != pVal->GetIndex() + pVal->GetCoeff()) && (l1.GetListLength() == 4));
}

TEST(TDatList, can_delete_current_link)
{
	TDatList l1;
	PTMonom pVal;
	for (int i = 0; i < 5; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	l1.SetCurrentPos(3);
	l1.DelCurrent();
	l1.SetCurrentPos(3);
	pVal = (PTMonom)l1.GetDatValue();
	EXPECT_TRUE((pVal->GetIndex() + pVal->GetCoeff() == 448) && (l1.GetListLength() == 4));
}

TEST(TDatList, can_delete_list)
{
	TDatList l1;
	PTMonom pVal;
	for (int i = 0; i < 5; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		l1.InsLast(pVal);
	}
	l1.DelList();
	EXPECT_TRUE((l1.IsEmpty()) && (l1.GetListLength() == 0) && (l1.GetDatValue() == nullptr));
}

TEST(TDatList, can_carry_out_multiple_operations)
{
	TDatList list_1;
	PTMonom pVal;
	int temp, length;
	for (int i = 1; i < 7; i++)
	{
		pVal = new TMonom(i, i * 100 + i * 10 + i);
		list_1.InsLast(pVal);
	}
	length = list_1.GetListLength();
	list_1.SetCurrentPos(4);
	list_1.GoNext();
	list_1.GoNext();
	list_1.SetCurrentPos(length - 4);
	for (int i = 0; i < 5; i++)
	{
		list_1.DelCurrent();
	}
	length = list_1.GetListLength();
	EXPECT_TRUE(length, 1);
}

int main(int argc, char *argv[])
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
